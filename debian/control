Source: golang-github-coredhcp-coredhcp
Section: admin
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-golang,
 golang-any,
 golang-github-bits-and-blooms-bitset-dev,
 golang-github-chappjc-logrus-prefix-dev (>= 0.0.0+git.20180227.3a1d648-1~),
 golang-github-fsnotify-fsnotify-dev,
 golang-github-google-gopacket-dev,
 golang-github-insomniacslk-dhcp-dev,
 golang-github-rifflock-lfshook-dev,
 golang-github-sirupsen-logrus-dev,
 golang-github-spf13-cast-dev,
 golang-github-spf13-pflag-dev,
 golang-github-spf13-viper-dev,
 golang-github-stretchr-testify-dev,
 golang-github-vishvananda-netns-dev,
 golang-golang-x-net-dev,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-coredhcp-coredhcp
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-coredhcp-coredhcp.git
Homepage: https://github.com/coredhcp/coredhcp
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/coredhcp/coredhcp

Package: coredhcp-client
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Static-Built-Using: ${misc:Static-Built-Using},
Description: multithreaded, modular and extensible DHCP server - client
 Coredhcp is a fast, multithreaded, modular and extensible DHCP server written
 in Go. In CoreDHCP almost everything is implemented as a plugin. Every request
 is evaluated calling each plugin in order, until one breaks the evaluation and
 responds to, or drops, the request.
 .
 This package contains the client.

Package: coredhcp-server
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Static-Built-Using: ${misc:Static-Built-Using},
Description: multithreaded, modular and extensible DHCP server - server
 Coredhcp is a fast, multithreaded, modular and extensible DHCP server written
 in Go. In CoreDHCP almost everything is implemented as a plugin. Every request
 is evaluated calling each plugin in order, until one breaks the evaluation and
 responds to, or drops, the request.
 .
 This package contains the server.

Package: golang-github-coredhcp-coredhcp-dev
Architecture: all
Multi-Arch: foreign
Section: golang
Depends:
 golang-github-bits-and-blooms-bitset-dev,
 golang-github-chappjc-logrus-prefix-dev (>= 0.0.0+git.20180227.3a1d648-1~),
 golang-github-fsnotify-fsnotify-dev,
 golang-github-google-gopacket-dev,
 golang-github-insomniacslk-dhcp-dev,
 golang-github-rifflock-lfshook-dev,
 golang-github-sirupsen-logrus-dev,
 golang-github-spf13-cast-dev,
 golang-github-spf13-pflag-dev,
 golang-github-spf13-viper-dev,
 golang-github-stretchr-testify-dev,
 golang-github-vishvananda-netns-dev,
 golang-golang-x-net-dev,
 ${misc:Depends},
Description: multithreaded, modular and extensible DHCP server - library
 Coredhcp is a fast, multithreaded, modular and extensible DHCP server written
 in Go. In CoreDHCP almost everything is implemented as a plugin. Every request
 is evaluated calling each plugin in order, until one breaks the evaluation and
 responds to, or drops, the request.
 .
 This package contains the library.
